package org.zefxis.clientserveur.coapplayground_pushnotification;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.CoAP.Type;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.observe.ObserveManager;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.json.simple.JSONObject;


public class CoapPushData extends CoapServer {
    
    private static final int COAP_PORT = 8893;
    private static final String IP_ADDRESS = "127.0.0.1";
    private static int LAT = 1; 
    private static int LON = 1; 
    private static String resource = null;
    private volatile Type type = null;
    private static int ObserverCount = 0;
    private static InetSocketAddress bindToAddress = null;
    private static InetSocketAddress source = null;
    private static byte[] token = null;
    
    
    public CoapPushData() throws SocketException{
    	
    	
    	NetworkConfig config = NetworkConfig.getStandard();
    	bindToAddress = new InetSocketAddress(IP_ADDRESS, COAP_PORT); 
    	type = Type.NON;
    	
    	long EXCHANGE_LIFETIME = config.getLong(NetworkConfig.Keys.EXCHANGE_LIFETIME);
    	System.out.println(" EXCHANGE_LIFETIME "+EXCHANGE_LIFETIME);
    }
    
    private void addEndpoints() throws SocketException{ 
    	
    	
        addEndpoint(new CoapEndpoint(bindToAddress)); 
      
    } 
    
    public static void main(String[] args){
    	
    	long timeWindow = 5; 
    	long startTime = System.currentTimeMillis();
    	CoapPushData serveur = null;
		try {
			
			
			serveur = new CoapPushData();
			CoapPushDataResource CoapResource = serveur.new CoapPushDataResource();
			serveur.add(CoapResource);
			serveur.addEndpoints();
    		serveur.start();
    		
    	
    		ObserveManager manager = new ObserveManager();
	    	
	    	
	    	
	    	
	    	//observingEndpoint.getObserveRelation(token)
    		NetworkConfig config = NetworkConfig.getStandard();
    		OnOffSchceduler scheduler = new OnOffSchceduler(config);
    		scheduler.start();
    		
    		while(true) {
    			
    			
    			if(scheduler.isTimeOn()){
    				
    				
    				CoapResource.resourceChange();
    	    		  
        		    long currentTime = System.currentTimeMillis();
        		    
        		    if(currentTime - startTime >= 2000) {
        		    	
        		    	System.out.println(CoapResource.getObserverCount());
        		    	startTime = System.currentTimeMillis();
        		    	
        		    }
    			}else {
    				
    				
    				synchronized (scheduler) {
						
    					try {
							scheduler.wait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
    			}
    			
    			
    			
    			
    			
    		}
    		
			
		}catch(SocketException e) {e.printStackTrace();}
    	
	}
    
    
    public class CoapPushDataResource extends CoapResource {

    	String dataPushed = null;
    	public CoapPushDataResource() { 
            
            // set resource identifier 
            super("datastream"); 
             
            // set display name 
            getAttributes().setTitle("bridgeNextClosure Resource"); 
            setObservable(true);
            setObserveType(type);
            setVisible(true);
            
        } 
    	
    	public void resourceChange(){
    		
    		
        	JSONObject jsonObject = new JSONObject();
        	jsonObject.put("lat", String.valueOf(LAT));
        	jsonObject.put("lon", String.valueOf(LON));
        	jsonObject.put("op_name", "bridgeNextClosure");
        	resource = jsonObject.toJSONString();
    		LAT++; LON++;
    		changed();
    		
    	}
    	
    	
        @Override 
        public void handleGET(CoapExchange exchange){ 
             
        	
        	
        	token = exchange.advanced().getRequest().getToken();
    		Response response = new Response(ResponseCode.CONTENT);
    		response.setPayload(resource);
    		response.setType(type);
    		response.setDuplicate(false);
    		source = exchange.advanced().getRelation().getSource();
    		// System.out.println(exchange.advanced().getRequest().getSource().toString());
    		exchange.respond(response);
    		
    		
    		
        } 
        
        @Override
        public void changed() {

    		super.changed();
    	}
        
        @Override
        public int getObserverCount() {
        	// TODO Auto-generated method stub
        	
        	return super.getObserverCount();
        }
        
        public boolean checkRelation() {
        	
        	
        	return false;
        }
    }
		   
}
