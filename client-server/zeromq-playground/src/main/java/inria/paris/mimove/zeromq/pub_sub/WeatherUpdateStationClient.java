package inria.paris.mimove.zeromq.pub_sub;

import java.util.StringTokenizer;
import org.zeromq.ZMQ;

public class WeatherUpdateStationClient {

	public static void main(String[] args) {

		ThreadSubscriber subscriber1 = new ThreadSubscriber("10001");

		subscriber1.start();

		/*try {

			Thread.sleep(5000);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		subscriber1.disconnect();
		try {
			System.out.println("Master Thread sleep");
			Thread.sleep(20000);
			System.out.println("Master wake up");

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		subscriber1.connect();*/

	}

	private static class ThreadSubscriber extends Thread {

		ZMQ.Socket subscriber = null;
		String filter = null;
		ZMQ.Context context = null;
		boolean connected = true;
		String name = null;

		public ThreadSubscriber(String filter) {

			context = ZMQ.context(1);

			// Socket to talk to server
			subscriber = context.socket(ZMQ.SUB);
			subscriber.connect("tcp://localhost:5556");
			this.filter = filter;
			this.name = "ThreadSubscriber-" + filter;
			System.out.println(this.name + " Collecting updates from weather server");

		}

		public void connect() {

			if (connected) {
				return;
			}

			subscriber.connect("tcp://localhost:5556");
			subscriber.subscribe(filter.getBytes());
			synchronized (this) {

				this.notify();
				connected = true;
			}
		}

		public void disconnect() {

			if (connected) {
				return;
			}
			subscriber.disconnect("tcp://localhost:5556");
			connected = false;

		}

		public void run() {

			subscriber.subscribe(filter.getBytes());

			// Process 10000 updates
			int update_nbr = 0;
			long total_temp = 0;
			for(update_nbr = 0; update_nbr < 999999999; update_nbr++) {

				if (!connected) {

					synchronized (this) {

						try{
							
							System.out.println(this.name + "Wait");
							this.wait();

						}catch (InterruptedException e){
							
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

				// Use trim to remove the tailing '0' character
				String string = subscriber.recvStr(0).trim();

				StringTokenizer sscanf = new StringTokenizer(string, " ");
				int zipcode = Integer.valueOf(sscanf.nextToken());
				int temperature = Integer.valueOf(sscanf.nextToken());
				int relhumidity = Integer.valueOf(sscanf.nextToken());
				String update = String.format("%05d %d %d", zipcode, temperature, relhumidity);
				System.out.println(this.name + "  " + update);
				total_temp += temperature;

			}
			System.out.println(this.name + " Average temperature for zipcode '" + filter + "' was "
					+ (int) (total_temp / update_nbr));
			subscriber.close();

		}

	}
}
