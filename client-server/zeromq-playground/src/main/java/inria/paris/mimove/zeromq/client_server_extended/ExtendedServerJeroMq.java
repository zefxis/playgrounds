package inria.paris.mimove.zeromq.client_server_extended;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

public class ExtendedServerJeroMq  {
	
	public static void main(String[] args) {
		
		
		
		Server server1 = new Server("server1");
		Server server2 = new Server("server2");
		server1.start();
		server2.start();
	}
	
	private static class Server extends Thread
    {
        private Context context;
        private String name = null;
      

        private Server (String name)
        {
            this.context = ZMQ.context(1);
            this.name = name;
        }

        @Override
        public void run(){
           

            //  Socket to talk to clients
            ZMQ.Socket responder = context.socket(ZMQ.REP);
            responder.connect("tcp://*:5560");

            while (!Thread.currentThread().isInterrupted()) {
                // Wait for next request from the client
                byte[] request = responder.recv(0);
                String req = new String(request);
                System.out.println(name+" received : "+req);

                // Do some 'work'
                try {
    				
                	Thread.sleep(1000);	
                	
    			}catch(InterruptedException e) {e.printStackTrace();}

                // Send reply back to client
                String reply = name+" World "+req;
                responder.send(reply.getBytes(), 0);
            }
            responder.close();
            context.term();
           
        }

    }


}
