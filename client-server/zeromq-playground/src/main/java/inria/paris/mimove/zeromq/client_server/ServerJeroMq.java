package inria.paris.mimove.zeromq.client_server;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

public class ServerJeroMq  {
	
	public static void main(String[] args) {
		
		ZMQ.Context context = ZMQ.context(1);

        //  Socket to talk to clients
        ZMQ.Socket server = context.socket(ZMQ.REP);
        server.bind("tcp://*:5555");

        while (!Thread.currentThread().isInterrupted()) {
            // Wait for next request from the client
            byte[] request = server.recv(0);
            String req = new String(request);
            System.out.println("Received : "+req);

            // Do some 'work'
            try {
				
            	Thread.sleep(1000);	
            	
			}catch(InterruptedException e) {e.printStackTrace();}

            // Send reply back to client
            String reply = "World "+req;
            server.send(reply.getBytes(), 0);
        }
        server.close();
        context.term();
	}


}
