package inria.paris.mimove.zeromq.push_pull;

import org.zeromq.ZMQ;

public class TaskWork implements Runnable {

	private String taskname;
	
    public static void main (String[] args) throws Exception {
    	
        new Thread(new TaskWork("TaskWork1")).start();
        new Thread(new TaskWork("TaskWork2")).start();
        new Thread(new TaskWork("TaskWork3")).start();
    }
    
    public TaskWork(String taskname){
    	
    	this.taskname = taskname;
    }

	public void run() {
		// TODO Auto-generated method stub
		
		ZMQ.Context context = ZMQ.context(1);

        //  Socket to receive messages on
        ZMQ.Socket receiver = context.socket(ZMQ.PULL);
        receiver.connect("tcp://localhost:5557");

        //  Socket to send messages to
        ZMQ.Socket sender = context.socket(ZMQ.PUSH);
        sender.connect("tcp://localhost:5558");
        
        //  Process tasks forever
        while (!Thread.currentThread ().isInterrupted ()) {
            String string = new String(receiver.recv(0)).trim();
            long msec = Long.parseLong(string);
            //  Simple progress indicator for the viewer
            System.out.flush();
            System.out.println(this.taskname+" "+string + '.');

            //  Do the work
            try {
				
				Thread.sleep(msec);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

            //  Send results to sink
            sender.send("".getBytes(), 0);
        }
        sender.close();
        receiver.close();
        context.term();
        
	}
    
}
