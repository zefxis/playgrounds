package inria.paris.mimove.get_start.zeromq;

import org.zeromq.ZMQ;

public class Version {

	public static void main(String[] args) {
		System.out.println(String.format(
				"Version string: %s \nVersion int: %d \nMajor Version: %s \nMinor Version: %s \nPatch Version: %s",
				ZMQ.getVersionString(), ZMQ.getFullVersion(), ZMQ.getMajorVersion(), ZMQ.getMinorVersion(),
				ZMQ.getPatchVersion()));

	}

}
