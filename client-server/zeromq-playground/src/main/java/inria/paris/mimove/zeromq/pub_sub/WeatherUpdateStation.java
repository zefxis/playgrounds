package inria.paris.mimove.zeromq.pub_sub;

import java.util.Random;

import org.zeromq.ZMQ;

public class WeatherUpdateStation {

	public static void main(String[] args) throws Exception {

		final PublisherThread publisherThread = new PublisherThread();
		publisherThread.start();

		while (true) {

			Thread.sleep(5000);
			publisherThread.disconnect();
			Thread.sleep(10000);
			publisherThread.connect();
		}

	}

}

class PublisherThread extends Thread {

	boolean connected = true;
	ZMQ.Context context = null;
	ZMQ.Socket publisher = null;

	public PublisherThread() {

		context = ZMQ.context(1);
		publisher = context.socket(ZMQ.PUB);
		publisher.bind("tcp://*:5556");
		publisher.bind("ipc://weather");
	}

	public void connect() {

		synchronized (this) {

			context = ZMQ.context(1);
			publisher = context.socket(ZMQ.PUB);
			publisher.bind("tcp://*:5556");
			publisher.bind("ipc://weather");
			connected = true;
			this.notify();

		}

	}

	public void disconnect() {

		if (!connected) {
			;
			;
		}
		connected = false;
		publisher.close();

	}

	public void run() {

		// Initialize random number generator
		Random srandom = new Random(System.currentTimeMillis());
		while (!Thread.currentThread().isInterrupted()) {

			if (!connected) {

				synchronized (this) {
					try {

						this.wait();

					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

			// Get values that will fool the boss
			int zipcode, temperature, relhumidity;
			zipcode = 10000 + random();
			temperature = srandom.nextInt(215) - 80 + 1;
			relhumidity = srandom.nextInt(50) + 10 + 1;

			// Send message to all subscribers
			String update = String.format("%05d %d %d", zipcode, temperature, relhumidity);
			System.out.println("Publish : " + update);
			publisher.send(update, 0);

		}
		publisher.close();
		context.term();

	}

	public int random() {

		int Min = 1;
		int Max = 3;
		return (int) (Min + (Math.random() * (Max - Min)));

	}

}
