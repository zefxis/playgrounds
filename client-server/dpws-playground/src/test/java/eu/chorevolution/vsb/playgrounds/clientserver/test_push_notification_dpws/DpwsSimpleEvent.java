package eu.chorevolution.vsb.playgrounds.clientserver.test_push_notification_dpws;


import org.json.simple.JSONObject;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.DefaultEventSource;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;

import eu.chorevolution.vsb.gm.protocols.dpws.DPWSDevice;


public class DpwsSimpleEvent extends DefaultEventSource{
	
	public final static String	DOCU_NAMESPACE	= DPWSDevice.DOCU_NAMESPACE;
	
	public DpwsSimpleEvent() {
		super("DpwsSimpleEvent", new QName("BasicServices", DOCU_NAMESPACE));
		// TODO Auto-generated constructor stub
		
		Element name = new Element(new QName("name",DOCU_NAMESPACE), SchemaUtil.TYPE_STRING);
		setOutput(name);
	}
	
	public void fireHelloWorldSimpleEvent(int eventCounter){
		
		final long temp = 129394 * eventCounter, lon = 145682 * eventCounter , lat = 74579 * eventCounter; 
		ParameterValue parameterValue = createOutputValue();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("lat", String.valueOf(lat));
		jsonObject.put("lon", String.valueOf(lon));
		jsonObject.put("temp", String.valueOf(temp));
		jsonObject.put("op_name", "bridgeNextClosure");
		String dataEvent = jsonObject.toJSONString();
		ParameterValueManagement.setString(parameterValue, "name", dataEvent);
		fire(parameterValue, eventCounter++, CredentialInfo.EMPTY_CREDENTIAL_INFO);
	}

	
}
