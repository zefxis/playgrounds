package eu.chorevolution.vsb.playground.websocket.experiment;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import eu.chorevolution.vsb.playgrounds.str.websockets.StartServerWebSocket;
import fr.inria.mimove.monitor.agent.ExperimentAgent;
import fr.inria.mimove.monitor.listener.AgentListener;
import fr.inria.mimove.monitor.util.MonitorConstant;

public class StartExperiment {

	public static boolean experimentRunning = true;
	public static boolean handleLastsignal = false;
	public static long experimentStartTime = 0l;
	public static Long msgCounter = (long) 0;
	public static ExperimentAgent experimentAgent = null;
	public static Exp waitDuration = null;
	public static Long averageMsgSize = 0L;
	public static int threadNumber = 5;
	public static MessageGenerator msgGen = null;
	

	public static void main(String[] args){
		// TODO Auto-generated method stub

		/*
		 * if (args.length < 3){
		 * 
		 * System.err.println("Missing arguments"); System.err.
		 * println("java -jar StartExperiment-jar-with-dependencies.jar duration threadNumber rate"
		 * ); System.exit(0); }
		 */

		/*
		 * Parameters.experimentDuration = Long.valueOf(args[0]); threadNumber =
		 * Integer.valueOf(args[1]); Parameters.msgSendParam = Double.valueOf(args[2]);
		 */
		
		Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String start_time = sdf.format(cal.getTime());
        
		Parameters.experimentDuration = 30 * 60 * 1000;
		threadNumber = 85;
		Parameters.msgSendParam = 2;

		experimentAgent = new ExperimentAgent("StartExperimentFRIDAY08032019_WC_85_2_30", "128.93.64.52",MonitorConstant.PortTimestamp3);
		waitDuration = new Exp(Parameters.msgSendParam);
		int counter = 0;
		msgGen = new MessageGenerator();
		StartServerWebSocket server = new StartServerWebSocket();
		server.start();
		try{

			Thread.sleep(30000);

		} catch (InterruptedException e) {

			e.printStackTrace();
		}

		experimentStartTime = System.currentTimeMillis();
		
		
		while (counter < threadNumber){

			StartSourceApplication source = new StartSourceApplication(server, waitDuration, averageMsgSize, msgGen);
			source.start();
			counter++;
			
		}

		new java.util.Timer().schedule(new java.util.TimerTask() {

			@Override
			public void run(){

				
				  // TODO Auto-generated method stub //
				 Calendar cal = Calendar.getInstance();
				 String finish_time = sdf.format(cal.getTime());
				 System.out.println("Start at : "+start_time);
				 System.out.println("Complete at : "+finish_time);
				 experimentRunning = false;
				 System.out.println("experimentDuration "+Parameters.experimentDuration);
				 System.out.println("msgCounter "+msgCounter);
				
			}
		}, Parameters.experimentDuration);
		
		
		

	}

}
