package eu.chorevolution.vsb.playgrounds.str.websockets;

import java.net.URI;
import java.net.URISyntaxException;



public class StartWebSocketConsummer extends Thread {

	WebSocketConsummer webSocketConsummer = null;

	// private MeasureAgent agent = null;

	public StartWebSocketConsummer(WebSocketConsummer webSocketConsummer) {

		this.webSocketConsummer = webSocketConsummer;
////		this.agent = new MeasureAgent("timestamp_2", System.currentTimeMillis(), MonitorConstant.M2,
//				MonitorConstant.timestamp_2_port_listener);
	}

	public void run() {

		webSocketConsummer.connect();
		while (true) {

			String msg = null;

			try{
				
				msg = webSocketConsummer.msgQueue.take();

				if (!msg.isEmpty()) {

//					String message_id = msg.split("-")[1];
//					String message = msg.split("-")[0];
//					agent.fire(""+System.currentTimeMillis()+"-"+message_id);
					System.out.println(msg);
				}

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
	
	public static void main(String[] args) {
		
		URI uri = null;
		try {
			
		uri = new URI("http://:"+9082);
			
		} catch (URISyntaxException e){e.printStackTrace();}
		WebSocketConsummer consummer = new WebSocketConsummer(uri);
		StartWebSocketConsummer startconsummer = new StartWebSocketConsummer(consummer);
		startconsummer.start();
		
	}

}
